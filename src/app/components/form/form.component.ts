import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  form!: FormGroup;

  constructor(private fb: FormBuilder,
              private validators: ValidadoresService) {
    this.crearForm();
    
   }

  ngOnInit(): void {
  }

  crearForm():void {
    this.form = this.fb.group({
      nombre: ['',[Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/), this.validators.noZaida, this.validators.noEveneser]],
      apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/), this.validators.noFarella, this.validators.noMeleneses, this.validators.noPrieto]],
      edad: ['',Validators.required]
    });
  }

  save():void{
    console.log(this.form.value);
    this.form.reset();
  }

  clean():void{
    this.form.reset();
  }

  get nameNoValid(){
    return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched;
  }
  get lastnameNoValid(){
    return this.form.get('apellido')?.invalid && this.form.get('apellido')?.touched;
  }
  get edadNoValid(){
    return this.form.get('edad')?.invalid && this.form.get('edad')?.touched;
  }
  get nombreNoZaida(){
    return this.form.get('nombre')?.errors?.['noZaida'];
  }
  get nombreNoEveneser(){
    return this.form.get('nombre')?.errors?.['noEveneser'];
  }

  get apellidoNoFarella(){
    return this.form.get('apellido')?.errors?.['noFarella'];
  }

  get apellidoNoMeleneses(){
    return this.form.get('apellido')?.errors?.['noMeleneses'];
  }

  get apellidoNoPrieto(){
    return this.form.get('apellido')?.errors?.['noPrieto'];
  }
}
